/* $Id: README.txt */

INTRODUCTION
------------

This module provides the functionality that content of a section will be hidden or shown up when a visitor clicks its heading. It's convenient for both the writer and readers to get an outline of an article. 

INSTALLATION
------------

Decompress the installation file into your Drupal modules directory 
(usually sites/all/modules, see http://drupal.org/node/176044 for more information).

Enable the ShrinkSections module: Administer > Site building > Modules
(admin/build/modules)

